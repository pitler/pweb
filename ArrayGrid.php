<?php


class ArrayGrid extends funciones
{
	/**
	 * Objeto con los objetos principales del sistema
	 * @var Object - Objeto con los objetos principales del sistema
	 */
	public $mainObj;
	
	/**
 	 * Nombre de la clase
 	 * @var String  - Nombre de la clase 
 	 */
 	public $className;
 	
 	/**
 	 * Nombre de la clase encriptado
 	 * @var String  - Nombre de la clase encriptado 
 	 */
 	public $encClassName;
 	
 	/**
 	 * Permisos de la clase
 	 * @var Array  - Contiene los permisos de la clase [0]::INSERTAR, [1]::ACTUALIZAR, [2]::BORRAR 
 	 */
 	public $permissions;
 	
 	/**
 	 * Datos de la tabla a leer 	 
 	 * @var Array	Datos de la tabla a leer
 	 */
 	public $tableData;
 	
 	public $tableName;
	
	
	/**
   * 
   * Constructor de la clase
   * @param funcionesObject $funciones Objeto con las funciones mas usadas
   * @param connectionObject $connection Objeto con la conexión
   * @param objectSql $sql Objeto sql
   */
	public function __construct($mainObj, $className, $tableName)
	{
		
		$this->mainObj = $mainObj;
		$this->className = $className;
		$this->tableName = $tableName;
		
		//Encriptamos el nombre de la clase
		$this->encClassName = rawurlencode ( $this->mainObj->security->encryptVariable ( 1, "", $this->className ) );
		
		//Traemos los permisos de seguridad por pantalla para el usuario
		$this->permissions = $this->mainObj->security->verifyAction($this->className, $this->mainObj);
		
		//Se agregan las librerias para el uso del jqGrid por medio del jaGrdArray y el calendario
		/*require_once 'src/jqSuite/jq-config.php';
		require_once ABSPATH . "php/jqGrid.php";		
		require_once ABSPATH . "php/jqGridArray.php";	*/
		require_once PWSYSUTILS . "jqSuite/php/jqGrid.php";
		require_once PWSYSUTILS . "jqSuite/php/jqGridArray.php";
		require_once PWSYSUTILS . "jqSuite/php/jqCalendar.php";
		
		$this->tableData = $this->getTableData();
		
	}
	
	/** 
	 * Función que nos regresa el código que se genera para pintar el grid
	 */	
	protected function getList()
	{
		
	}

	/** 
	 * Función que nos regresa el código que se genera al recargar el grid 
	 * internamente, en este caso no hace nada asi que lo matamos
	 */
	protected function getGrid()
	{
		die();
	}
	
	/**	  
	 * Función encargada de ejecutar actualizaciones en la tabla usada por el grid	 * 
	 */
	protected function doUpdate($skipFields = false)
	{
	    
		$consulta = false;
		$content = "";
		//Verificamos si se tienen permisos para actualizar
		if(!$this->permissions["ACTUALIZAR"])
        {
        }
		
		//Leemos el array y armamos el query con las llaves y los datos
		$keyFields = array();
		$datos = array();
				
		$size = sizeof($this->tableData);
		$cont = 0;		

		foreach ($this->tableData as $columnName =>$tableItems)
		{
			
			//Esto es por default para identificar los sistemas
			//El valor 2 debe de ser una variable de sesión
			/*if($columnName == "CVE_SISTEMA")
			{
				$value = SYSID;
			}
			else 
			{
				$value = $this->getPVariable ( $columnName );	
			}			*/
			$value = $this->getPVariable ( $columnName );
			//Armamos el array con las llaves
			if($tableItems["key"] == 1)
			{
				$keyFields[$columnName] = $value;
			}
			
			//Si no es llave se agrega al String de los datos a actualizar
			else
			{
			    
			    
    			//Si es una fecha, depende del motor de base de datos le damos el formato
				if($tableItems["type"] == "date")
				{
				    switch (DBASE)
				    {
				    case 1 : 
				        $value = $this->mainObj->date->getMysqlDate($value);
				        break;
				    default :
				        break;
				    }
			    }
			    
			    //Esto es para fechas DATETIME de SQL , se manda en formato YYYYmmdd 
			    if($tableItems["type"] == 'datetime')
			    {

			        $value =  $this->mainObj->date->getSqlDate($value);
			        $value = str_replace("-", "", $value);
			    }
			    
				if($tableItems["type"] == 'smalldatetime')
			    {

			        $value =  $this->mainObj->date->getSqlSmallDate($value);
			        //$value = str_replace("-", "", $value);
			    }
			    

			    if(is_array($skipFields) && in_array($columnName, $skipFields))
			    {
			    	continue;
			    
			    }
			    else
			    {
			    	$datos[$columnName] = $value;
			    	//error_log("$columnName :: $value");
			    }
			    
			  //  $datos[$columnName] = $value;
			}
			$cont++;			
		}
		
		//Se agrega esto si el último elemento del array es una "," y se sutituye por un ""
		/*$datos .= "ENDDATA__";		
		
		$datos = preg_replace("/, ENDDATA__/", "", $datos);
		$datos = preg_replace("/ENDDATA__/", "", $datos);*/
		
		//Si el string es diferente a un espacio en blanco
		//Si el array de llaves lleva al menos 1 valor
		//Ejecutamos el update
		if($datos && sizeof($keyFields) >= 1)
		{	
			//error_log("Ejecuto conslta");
		    

		//	$consulta = $this->sql->updateData ( $this->connection, $this->tableName, $datos, $keyFields, false, true, $this->className );
			$consulta =  $this->mainObj->sql->updateData($this->mainObj->connection, $this->tableName, $datos, $keyFields);

		}
		

	  /*if(!$consulta)
    {
        $this->funciones->getValidation($this->className, 14);
        return;        
    }*/
	}	
	
	/**
	 * 
	 * Función encargada de insertar nuevos registros en la tabla usada por el grid
	 * @param $consecutivos	Array	Array que contiene los campos para generar un consecutivo
	 * @param $valida		Boolean	Si es true, validamos que la llave no se duplique
	 * @param $skipFields	Array	Campos a ignorar para incluir en el query, se usa para llaves autoincrement
	 * @param $numValues	Array	Array con los campos que queremos que sean solo números
	 */
	protected function doInsert($consecutivos = false, $valida = false, $skipFields = null, $numValues = null, $validaKeys = false )
	{

	    $keyResult = true;
		
		//Verificamos si se tienen permisos para actualizar
		if (!$this->permissions["INSERTAR"])
		{
			/*$this->funciones->getValidation ( $this->className, 22 );
			return;*/
		}				
		
		//Leemos el array y armamos el query con las llaves y los datos		
		$consulta = false;
		$content = "";		
		$datos = "";
		$fields = "";
		$keyfieldsAux = null;

		if($skipFields)
		{
		    foreach ($skipFields as $sField)
		    {		        
		        if(isset($this->tableData[$sField]))
		        {
		            unset($this->tableData[$sField]);
		        }
		    }
		}
		
		$size = sizeof($this->tableData);
		$cont = 0;
		$params = array(); 
		
		$keyFields = null;        
		
		foreach ($this->tableData as $columnName =>$tableItems)
		{
		    
		  //Esto es por default para identificar los sistemas
		  //El valor 2 debe de ser una variable de sesión
			
		
				//Checo el array de consecutivos para ver si cargo los datos 
				//de POST o de la funcion  getConsecutivo() o getConsecutivoStr()
				if(isset($consecutivos[$columnName]) && $consecutivos[$columnName]["TYPE"] >=  1)
				{		
				    
					$consData = $consecutivos[$columnName];

					//Checo el tipo de funcion que envio												
					//Para 1 es getConsecutivo()
					if($consData["TYPE"] == 1)
					{						
						$value = $this->mainObj->system->getConsecutivo ( $this->mainObj, $this->tableName,$columnName, 0);
					}
				}				
				else
				{
					$value = $this->getPVariable ( $columnName );
					
					//Si es una fecha, depende del motor de base de datos le damos el formato
					if($tableItems["type"] == "date")
					{
					    switch (DBASE)
					    {
					    case 1 : 
					        $value = $this->mainObj->date->getMysqlDate($value);
					        break;
					    default :
					        break;
					    }
					    
					}
					
		
				}	
			

			//Se agrega el nombre de la columna a insertar			
			$fields .= "$columnName";	
			//Se agrega el valor de la columna a insertar
			$datos .= "?";

			//Esto es para que solo acepte numeros en los campos con ,
			if(is_array($numValues)){
				if(in_array($columnName,$numValues))
				{
				   // error_get_last("Entro a quitar:  $columnName");
				    $value = preg_replace("/,/", "", $value);
				}
			}
			
			//Llenamos el array para nuevos Keys
			if($validaKeys)
			{
			    if(in_array($columnName, $validaKeys))
			    {
			        $keyfieldsAux[$columnName] = $value;
			    }

			    
			}
			
			
			$params[] = $value;
				//Se agrega una coma para separarlos siempre y cuando no se ael último elemento
			if($cont < $size-1)
			{
				$datos .= ", ";
				$fields .= ", ";
			}			
			
			if($tableItems["key"] == 1)
			{
			    $keyFields[$columnName] = $value ;
			    /*if($returnKey)
			    {
			        $keyResult = $value;
			    }*/
			}
			
			$cont++;			
		}

		if($valida != false)
		{
		    //Es valido por default
		    //Si keyFields no viene vacio y skipFields es nulo, revisamos

		    $valid = true;
		    
		    if(isset($validaKeys) && $keyfieldsAux)
		    {
		        $keyFields = $keyfieldsAux;
		    }
		    if(isset($keyFields))
		    {   
		        $valid = $this->mainObj->system->validaInsert($this->mainObj, $keyFields, $this->tableName, $this->className);
		        
		    }
		    if(!$valid)
		    {   
		        return "insertFalse";
		    }
		}
		
		
		if(trim($datos) != "" && trim($fields) != "")
		{
			$result = $this->mainObj->sql->insertData($this->mainObj->connection, $this->tableName, $fields, $datos, $params, $this->className);
			/*if(!$result)
			{
			    $keyResult = false;
			}*/
			
		}
		
		return $keyResult;
	}
	
	/**
	 * 
	 * Función encargada de borrar los registros en la tabla usada por el grid 
	 */
	protected function doDelete()
	{
		$consulta = false;
		$content = "";		
		//Verificamos si se tienen permisos para actualizar
		if (!$this->permissions["BORRAR"])
		{
			/*$this->funciones->getValidation ( $this->className, 22 );
			return;*/
		}				
		
		//Leemos el array y armamos el query con las llaves y los datos
		$keyFields = array();
		$datos = "";
				
		foreach ($this->tableData as $columnName =>$tableItems)
		{			
			//Esto es por default para identificar los sistemas
			//El valor 2 debe de ser una variable de sesión
			if($columnName == "CVE_SISTEMA")
			{
				$value = SYSID;
			}
			else
			{			    
				$value = $this->getPVariable ( $columnName );				
			}	
			
			//Armamos el array con las llaves
			if($tableItems["key"] == 1)
			{
				$keyFields[$columnName] = $value;
			}			
		}

		//Si el array de llaves lleva al menos 1 valor
		//Ejecutamos el delete
		if(sizeof($keyFields) >= 1)
		{
		
        	$sqlResult = $this->mainObj->sql->deleteData($this->mainObj->connection,$this->tableName,$keyFields);		
		}
		
	}
	
	/**
	 * 
	 * Función encargada de generar el archivo de excel para el grid
	 */
	protected function generaExcel()
	{
		
	}	
	
	private function getTableData()
	{
		
		include_once (PWSYSLIB."db/DBClassGenerator.php");
		
		$classObject = new DBClassGenerator($this->mainObj, $this->className, $this->tableName);

		$data = $classObject->verifyClass();
		
		if(!$data)
		{
			$result = $classObject->createClass();		
			if($result === false)
			{
				//Mando error de creacion de fichero
				error_log("No se puede generar el objeto de la tabla");
			}	
		}
		
		$data = $classObject->getClassContent();
		
		return $data;		
	}	
}
?>