 <?
 
 /**
   * Clase que se encarga de la autenticación al sistema:
   * funciones : se encarga de las funciones mas usuadas en el sistema
   * conexion : objeto con la conexión a la base de datos	    
   *
   */
 
 class login  extends funciones
 {

 	/**
 	 * Objeto con las variables principales
 	 * @var mainVarsObject Objeto con las variables principales 
 	 */ 	
 	private $mainObj;
 	
 	/**
 	 * Nombre de la clase
 	 * @var String Nombre de la clase 
 	 */
 	private $className;
 	
 	function __construct($mainObj)
 	{ 		
 		$this->mainObj = $mainObj;	 		
 		$this->className = "login";
 	}


 	public function getLogin($mode = false, $reqCaptcha= false)
 	{	 	    
 		
 	  $tempError = "";
 			
 		switch ($mode)
 		{
 		    
 			case 1: 				
 				$data = $this->getTemplate("login");
 				
 				//$data = preg_replace("/__MAINCSS__/", SITECSSPATH, $data);
 				$data = preg_replace("/__PAGETITLE__/", PAGETITLE, $data);
 				$captcha = "";
 				if($reqCaptcha == true)
 				{
 					$captcha = $this->getTemplate("captcha");
 				}
 				$data = preg_replace("/__CAPTCHA__/", $captcha, $data);
 				$errorMsg = "";
 				if(isset($_SESSION["error"])) 				
				{
					$tempError = $this->getTemplate("loginError");
					$errorMsg = $this->getErrorMessage($_SESSION["error"]);	
					$tempError = preg_replace("/__ERROR__/", $errorMsg, $tempError);
					$_SESSION["error"] = null;
				}
				$data = preg_replace("/__ERRORMSG__/", $tempError, $data);
 				break;
 		 
		 default:
 		  $data = $this->doVerify($reqCaptcha);
 		 break;
 		}

 		return $data;
 	}
 	
 	
 	public function getSiteLogin($reqCaptcha, $loginMode = false, $errorNumber = false, $module = "")
 	{
 		
 		if($loginMode == false)
 		{
 			$loginMode = $this->getPVariable("loginMode");
 		}
 		
 		if(!$loginMode)
 		{
 			$loginMode = 1;
 		}
 		
 		if(isset ( $_SESSION ["autentified"]) &&  $_SESSION ["autentified"]  != null)
 		{
 			
 			$this->reloadPage("inicio");
 		}
 		
 		//Presento pantalla de login
 		if($loginMode == 1)
 		{
 			$data = $this->getTemplateLogin($reqCaptcha, $errorNumber, $module);
 			return $data;
 		
 		}
 		
 		//Hago la validación
 		if($loginMode == 2)
 		{
 			$data = $this->doVerify($reqCaptcha, true, $module);
 			return $data;
 		}
 	}
 	
 	public function getTemplateLogin($reqCaptcha, $errorNumber = null, $moduleName = "")
 	{
 		//$data  = 	file_get_contents('template/login.html', true);
 		$data  = 	file_get_contents('template/login.html', true);
 		
 	
 		$error = ""; 		
 		if($errorNumber == null)
 		{
 			$error = "";
 		}
 		else
 		{
 			$error = $this->getTemplate("loginError");
 			$errorMsg = $this->getErrorMessage($errorNumber);
 			$error = preg_replace("/__ERROR__/", $errorMsg, $error);
 		}
 	
 		
 		$data = preg_replace("/__ERROR__/", $error, $data);
 		$data = preg_replace("/__MODULE__/", $moduleName, $data);
 	
 		return $data;
 	}
 	
 /**
  * Verifica si el usuario está autorizado para entrar al sistema
  * Regenera la sesión para que no sea la misma con la que entramos a la página
  * Hace validaciones de captcha y luego con la base de datos
  * Si la informacion es correcta genera la sesión y cifra los datos
  * Si la autenticación es por medio de ldap manda a llamar ldapVerify
  * @param Boolean $reqCaptcha Si se necesita el captcha 
  */
  private function doVerify($reqCaptcha = false, $site = false, $module = "")
 	{	
 		
 		$login = $this->getPVariable("usuario");
 		$password = $this->getPVariable("password");
 		
        /****COMENTAMOS PARA PRUEBAS****/
 		/*if($site == 1)
 		{
 			$recaptchaResponse  = $this->getPVariable ( "g-recaptcha-response" );
 			$captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LetiCkTAAAAAFaRtYlGfeXOx9UjG4Vb9B22NyQC&response='.$recaptchaResponse.'&remoteip='.$_SERVER['REMOTE_ADDR']),TRUE);
 		}
 		else
 		{
 			$captcha['success']= true;
 		}
 			
 		if($reqCaptcha)
 		{
 			$captcha = $this->getPVariable("captcha");
 		}
 			
 		if($captcha['success']=== true){
 			//Captcha correcto
 			$reqCaptcha = false;
 				
 		} else
 		{
 			//
 				
 			$_SESSION ["error"] = 24;
 			$this->setError ( 24, null, $this->className, 2 );
 			$msg = $this->getErrorMessage(24);
 			//$this->mainObj->sql->insertaBitacora($this->mainObj->connection, $login, "login", "", "Inicio de sesión", "Error: $msg", "CVFSYSW");
 			//return $this->getLogin ( 1,$reqCaptcha);
 			return $this->getSiteLogin($reqCaptcha, 1, 24, $module);
 		}*/
 		
 	    /**** FIN COMENTAMOS PARA PRUEBAS****/
 			
 		
 	
 		
 		//Regeneramos la sesión  y borramos la anterior
		session_regenerate_id(true);
 		$data = "";

 		//No tiene ni login ni password
 		if(!$login && !$password)
 		{

 			$_SESSION["error"] = 21;
 			$this->setError(21, null ,$this->className,2);
 			if($site == true)
 			{
 				return $this->getSiteLogin($reqCaptcha, 1, 21, $module); 				
 			}
 			else
 			{
 			return $this->getLogin(1, $reqCaptcha);
 			}
 		}

 		//No tiene login
 		else if(!$login && $password)
 		{
 			$_SESSION["error"] = 22;
 			$this->setError(22, null, $this->className, 2);
 			if($site == true)
 			{
 				return $this->getSiteLogin($reqCaptcha, 1, 22, $module); 				
 			}
 			else
 			{
 			return $this->getLogin(1, $reqCaptcha);
 			}
 		}

 		//No tiene password
 		else if($login && !$password)
 		{
 			$_SESSION["error"] = 23;
 			$this->setError(23, null , $this->className, 2);
 			if($site == true)
 			{
 				return $this->getSiteLogin($reqCaptcha, 1, 23, $module); 				
 			}
 			else
 			{
 			return $this->getLogin(1, $reqCaptcha);
 			}
 		}
 		
 		$fields = "";
 	if($site == true)
    {
    	$tabla = "SITE_USUARIOS";
    	$fields = array ("CVE_USUARIO", "NOM_USUARIO", "LLAVE", "STATUS", "LAST_LOGIN", "LANG", "IMAGEN", "VIGENCIA", "FECHA_CAMBIO", "INACTIVIDAD", "CODE", "INTENTOS", "LAST_ACTIVITY",);
    	//$fields = array ("CVE_USUARIO", "NOM_USUARIO", "CVE_CLIENTE", "LLAVE", "STATUS", "LAST_LOGIN", "LANG", "IMAGEN", "VIGENCIA", "FECHA_CAMBIO", "INACTIVIDAD", "CODE", "INTENTOS", "LAST_ACTIVITY", "RAIZ", "MAKER_CHECKER" );
    }
    else 
    {
    	$tabla = "SYS_USUARIOS";
    	$fields = array ("CVE_USUARIO", "NOM_USUARIO", "CVE_PERFIL", "LLAVE", "STATUS", "LAST_LOGIN", "LANG", "IMAGEN", "VIGENCIA", "FECHA_CAMBIO", "INACTIVIDAD", "CODE", "INTENTOS", "LAST_ACTIVITY" );
    }
    
        $condition = array("CVE_USUARIO" => $login);		
		$userInfo = $this->mainObj->sql->executeQuery($this->mainObj->connection, $tabla, $fields, $condition);	

		$this->getVardumpLog($userInfo);

		
 		if($userInfo)
 		{
 		    
 		   // $this->createSession($userInfo, $password, false, false, $site, $module); 		
 		    
 			$userInfo = $userInfo[0];
 			//si es un usuario nuevo, lo mandamos a recuperar su password


 			if ($userInfo ["STATUS"] == 4)
 			{
 				
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className, "Acceso de nuevo usuario", "");
 				
 				$codigo = $userInfo["CODE"];
 				if($site == true)
 				{
 					//error_log("Login :: Recargo para site");
 					
 				    
 				    if(trim($userInfo["LLAVE"]) == md5(trim($password)))
 				   // if(trim($userInfo["LLAVE"]) == hash("sha256", trim($password))) 				        
 				    {
 				        $this->reloadPage("recover&code=$codigo&t=nw");
 				    }
 				    else 
 				    {
 				        
 				        $_SESSION ["error"] = 29;
 				        $this->setError ( 29, "Clase : " . $this->className );
 				        $msg = $this->getErrorMessage(29);
 				        return $this->getSiteLogin($reqCaptcha, 1, 29, $module); 	
 				        
 				    }
 				    
 				    
 				    
 					
 				}
 				else
 				{
 					//error_log("Login :: Recargo para admin");
 					echo header ( "Location:recover.php?code=$codigo" );
 				} 				
 				return false;
 			}
 			
 			$fechaCambio = $userInfo ["FECHA_CAMBIO"];
 			$vigencia = intval($userInfo ["VIGENCIA"]);
 			
 			
 			
 			$hoy = date("Ymd");
 			$diferencia = $this->mainObj->date->diferenciaDias($hoy, $fechaCambio);
 			//error_log("Diferencia : $diferencia :: $vigencia :: $fechaCambio");
 			
 			if($diferencia >= $vigencia)
 			{
 				$_SESSION["error"] = 317;
 				$this->setError(317, __FUNCTION__, __CLASS__);
 				
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(317), "");
 				if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 317, $module);
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 	
 			}
 			
 			
 			//Si ya existe una sesion activa
 			if($userInfo["STATUS"] == 3)
 			{

 				$fechaGuardada = date_create($userInfo["LAST_ACTIVITY"]);
 				$fechaActual = date_create(date("Y-m-d H:i:s"));
 				$interval = date_diff($fechaGuardada, $fechaActual);
 				$min=$interval->format('%i');
 				$hora=$interval->format('%H');
 				$dias=$interval->format('%d');
 				
 				//error_log("Intervalo :: $min :: $hora :: $dias");
 				//Si el tiempo de inactividad es mayor a 20 min, lo dejo pasar de nuevo
 				if($min >= 20 || intval($hora) >= 1 || intval($dias) >= 1)
 				{
 					$datos = array("STATUS" => 1);
 					$keyFields = array("CVE_USUARIO" => $userInfo["CVE_USUARIO"]);
 					$consulta =  $this->mainObj->sql->updateData($this->mainObj->connection, $tabla, $datos, $keyFields);
 					
 					if($site == true)
 					{
 						return $this->getSiteLogin($reqCaptcha, 1);
 					}
 					else
 					{
 						return $this->getLogin(1, $reqCaptcha);
 					}
 				}
 				
 				
 				$_SESSION["error"] = 27;
 				$this->setError(27, __FUNCTION__, __CLASS__);
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(27), "");
 				if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 27, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 			}
 			
 			//si está bloqueado
 			if ($userInfo ["STATUS"] == 5)
 			{
 			
 				$_SESSION ["error"] = 154;
 				$this->setError ( 154, "Clase : " . $this->className );
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(154), "");
 			  if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 154, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 			}
 			
 			//Bloqueado por inactividad
 			if ($userInfo ["STATUS"] == 8)
 			{
 				$_SESSION ["error"] = 157;
 				$this->setError ( 157, "Clase : " . $this->className );
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(157), "");
 			  if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 157, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 			}
 			
 			//Eliminado por el usuario padre
 			if ($userInfo ["STATUS"] == 10)
 			{
 			    $_SESSION ["error"] = 158;
 			    $this->setError ( 158, "Clase : " . $this->className );
 			    $this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(158), "");
 			    if($site == true)
 			    {
 			        return $this->getSiteLogin($reqCaptcha, 1, 158, $module);
 			    }
 			    else
 			    {
 			        return $this->getLogin(1, $reqCaptcha);
 			    }
 			}
 			
 			$lastLogin = $this->lastLogin($userInfo);
 			if($lastLogin == false )
 			{
 				$this->blockUser($userInfo,$site,$reqCaptcha , 8);
 				$_SESSION ["error"] = 157;
 				$this->setError ( 157, "Clase : " . $this->className );
 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(157), "");
 				//return $this->getLogin ( 1,$reqCaptcha );
 				if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 157, $module);
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 			}
 			//si está desactivado 
			if($userInfo["STATUS"] == 0)
			{
				$_SESSION["error"] = 28;
				$this->setError(28, __FUNCTION__, __CLASS__);
				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(28), "");
				if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 28, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
			}
			
			if(trim($userInfo["LLAVE"]) == md5(trim($password)))
			//if(trim($userInfo["LLAVE"]) == hash("sha256", trim($password)))			    
 			{ 				
 			//	error_log("Mando a crear la sesion");
 				$this->createSession($userInfo, $password, false, false, $site, $module); 				
 			}
 			
 			//Password Incorrecto
 			else
 			{
 				$tabla = "";
 				if($site == true)
 				{
 					$tabla = "SITE_USUARIOS";
 				}
 				else
 				{
 					$tabla = "SYS_USUARIOS";
 				}
 				
 				$_SESSION ["error"] = 29;
 				$this->setError ( 29, "Clase : " . $this->className );
 				$msg = $this->getErrorMessage(29);

 				$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $msg, "");
 				
 				$intentos = $userInfo["INTENTOS"] + 1;
 				$datos = array("INTENTOS" => $intentos);
 				$keyFields = array("CVE_USUARIO" => $userInfo["CVE_USUARIO"]);
 				
 				$this->mainObj->sql->updateData($this->mainObj->connection, $tabla, $datos, $keyFields);
 				
 				if($intentos >= 3)
 				{
 					$this->blockUser($userInfo, $site, $reqCaptcha);
 					$_SESSION ["error"] = 154;
 					$this->setError ( 154, "Clase : " . $this->className );
 					$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], $this->className,  $this->getErrorMessage(154), "");
 				}
 				 
 				
 				if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 29, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 				
 			} 			
 			
 		}
 		//Login incorrecto
 		else
 		{
	 		$_SESSION["error"] = 26;
	 		$this->setError(26, __FUNCTION__ , __CLASS__);

 		if($site == true)
 				{
 					return $this->getSiteLogin($reqCaptcha, 1, 26, $module); 				
 				}
 				else
 				{
 					return $this->getLogin(1, $reqCaptcha);
 				}
 		}
 	}
 	
 
	/**
	 * Una vez que las credenciales son autenticadas, creamos la sesión
	 * Creamos una sessionKey
	 * Encriptamos la información que ira en la cookie
	 * Quitamos el captha
	 * Guardamos la fecha deconexión del usuario
	 * @param Array	 	$userInfo	Datos del usuario 
	 * @param Array	 	$password	Password del usuario
	 * @param Boolean $isLdap		Si es conexión por LDAP
	 * @param Object	$ldapConn	Objeto conexión LDAP
	 */	
	private function createSession($userInfo, $password, $isLdap = false, $ldapConn = false, $site = false, $module = "")
 	{


 		$this->mainObj->security->createSessionKey();
 		//Vamos por el perfil
 		if($site == true)
 		{
 			//$tabla = "SITE_PERFILES";
 		    $userInfoAux = true;
 		}
 		else 
 		{
 			$tabla = "SYS_PERFILES";
 			$condition = array("CVE_PERFIL"=> $userInfo["CVE_PERFIL"]);
 			$fields = array("ID","DESC_PERFIL");
 			$userInfoAux = $this->mainObj->sql->executeQuery($this->mainObj->connection, $tabla, $fields, $condition); 			
 		}
 				
		
		if($userInfoAux)
		{
		    
		    if($site == true)
		    {
		        $this->mainObj->security->encryptVariable(2, "raiz", $userInfo["RAIZ"]);
		        $this->mainObj->security->encryptVariable(2, "cveCliente", $userInfo["CVE_CLIENTE"]);
		        
		        $entidad = array();
		        $conditionEntidad = array("CVE_CLIENTE" => $userInfo["CVE_CLIENTE"]);
		        $sqlEntidad = $this->mainObj->sql->executeQuery($this->mainObj->connection, "ENTIDAD_CLIENTE", null, $conditionEntidad);
		        if($sqlEntidad)
		        {
		            foreach ($sqlEntidad as $entidadItem)
		            {
		                $entidad[] = $entidadItem["CVE_ENTIDAD"];
		            }
		        }
		        $this->mainObj->security->encryptVariable(2, "entidad", serialize($entidad));
		        
		    }
		    else 
		    {
		        $userInfoAux = $userInfoAux[0];
		        $this->mainObj->security->encryptVariable(2, "descPerfil", $userInfoAux["DESC_PERFIL"]);
		        $this->mainObj->security->encryptVariable(2, "idPerfil", $userInfoAux["ID"]);
		        $this->mainObj->security->encryptVariable(2, "cvePerfil", $userInfo["CVE_PERFIL"]);		        
		    }
				  	
			//Encriptamos las variables
		    $this->mainObj->security->encryptVariable(2, "mkr", $userInfo["MAKER_CHECKER"]);
			$this->mainObj->security->encryptVariable(2, "nombre", $userInfo["NOM_USUARIO"]);
 			$this->mainObj->security->encryptVariable(2, "cveUsuario", $userInfo["CVE_USUARIO"]);
			$this->mainObj->security->encryptVariable(2, "llave", $password);
			$this->mainObj->security->encryptVariable(2, "lastAccess", date("Y-n-j G:i:s"));
			//$this->mainObj->security->encryptVariable(2, "imagen", $userInfo["IMAGEN"]);
			
			//Guardamos la ultima actividad			
			//strtotime($time)
			
			$_SESSION["activity"] = date("Y-m-d H:i:s");
			
			$_SESSION["error"] = null;

			//Fingerprint del navegador y su ip
			$_SESSION["fingerPrint"] = md5($_SERVER['HTTP_USER_AGENT']);
			$_SESSION["remoteAddr"] = md5($_SERVER['REMOTE_ADDR']);
			unset($_SESSION["captcha"]);

			if($site == true)
			{
				$tabla = "SITE_USUARIOS";
				
			}
			else
			{
				$tabla = "SYS_USUARIOS";
				
			}
			
			//Guardamos el ultimo inicio de sesion
		//	$hoy = date("Y-m-d H:i:s");		

			$fechaActual = date("Y-m-d H:i:s");
			$driver = $this->getSqlDriver($this->mainObj->connection);
			if($driver == "sqlsrv")
			{
				$fechaActual = str_replace("-", "", $fechaActual);
			}
			
			$datos = array("LAST_LOGIN" =>  $fechaActual, "STATUS" => 3, "INTENTOS" => 0);
			$keyFields = array("CVE_USUARIO" => $userInfo["CVE_USUARIO"]);
			
			$consulta =  $this->mainObj->sql->updateData($this->mainObj->connection, $tabla, $datos, $keyFields);
			$this->mainObj->sql->insertaBitacora($this->mainObj->connection,  $userInfo["CVE_USUARIO"], "login", "Inicio de sesión exitoso", "");
			
			
			
			//Si es LDAP cerramos la conexión
			/*if($isLdap)
			{
				ldap_close($ldapConn);
			}*/
			
			if($module == "login")
			{
				$module = "clientes";
			}
 			return $this->reloadPage($module);
 		}
 		else
 		{
 			$this->setError(51, __FUNCTION__, __CLASS__,2);
 		}		
 	}
 	
 	/**
 	 * Bloquea al usuario por fallo de password
 	 * @param array() $cveUser Información del Usuario
 	 */
 	private function blockUser($userInfo, $site, $reqCaptcha, $status = 5 )
 	{
 	
 		$tabla = "";
 		if($site == true)
 		{
 			$tabla = "SITE_USUARIOS";
 		}
 		else
 		{
 			$tabla = "SYS_USUARIOS"; 			
 		}
 		
 		$datos = array("STATUS" => $status, "INTENTOS" => 0);
 		$keyFields = array("CVE_USUARIO" => $userInfo["CVE_USUARIO"]); 	
 	
 		$consulta =  $this->mainObj->sql->updateData($this->mainObj->connection, $tabla, $datos, $keyFields);
 	
 		$this->setError ( 154, "Clase : " . $this->className );
 		//$msg = $this->getErrorMessage(154);
 		//$this->mainObj->sql->insertaBitacora($this->mainObj->connection, $userInfo["CVE_USUARIO"], "login", "", "Bloqueo de usuario por intentos fallidos", "Error: $msg", "web");
 	
 	
 		if($status == 5)
 		{
 			if($site == true)
 			{
 				return $this->getSiteLogin($reqCaptcha, 1, 154, $module);
 			}
 			else
 			{
 				return $this->getLogin(1, $reqCaptcha);
 			}
 			//return $this->getLogin ( 1, false, true );
 		}
 		if($status == 8)
 		{
 			return true; 	
 		}
 	}
 	

 	/**
 	 * Función que verifica que el usuario no tenga mas de 45 dias sin conectarse
 	 * Si tiene mas que eso lo bloquea
 	 * @param Array $userInfo	Array con los datos del usuario
 	 * @return boolean
 	 */
 	private function lastLogin($userInfo)
 	{
 		
 		$dias = 0;
 		$hoy = date("Ymd");
 		$result = true;
 		$lastLogin = $userInfo["LAST_LOGIN"];
 		$dias = $userInfo["INACTIVIDAD"];
 		//$lastLogin = "20160515";
 		if($lastLogin == null || $lastLogin == "")
 		{
 			$lastLogin = $hoy;
 		}
 		$diferencia = $this->mainObj->date->diferenciaDias($hoy, $lastLogin);
 		 
 		if(intval($diferencia) >= intval($dias))
 		{
 			$result = false;
 		}
 		return $result;
 	}

 	function getTemplate($name)
 	{
 	    
 	    $template["login"] = <<<TEMPLATE
 	    <!DOCTYPE html>
<html class="bg-white" >
    <head>
        <meta charset="UTF-8">
        <title> .:: __PAGETITLE__ ::. </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta http-equiv="PRAGMA" content="NO-CACHE">
  			<meta http-equiv="Expires" content="-1" />
				<meta name="robots" content="noindex, nofollow,noarchive,noydir" />
				<meta name="creator" content="π-tLeR" />		
				<meta name="language" content="es" />
				<meta name="identifier-url" content="" />

				<!--Carga elementos nuevos para explorer < 8 -->
  			<!--[if lte IE 8]>
  			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  			<![endif]-->
  			
  			<!-- Javascripts -->
  			<script src="pw/assets/plugins/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>
  			<!-- Bootstrap -->
        <script src="pw/assets/plugins/bootstrap3.0.3/js/bootstrap.min.js" type="text/javascript"></script>
  			
  			
  			<!-- PARA EL CAPTCHA -->
  			<!--<noscript>
  			<div class="div_NOSCRIPT">
        		<p>Para que este web site funcione correctamente, es necesario habilitar javascript en tu navegador.</p>
        		<p>
        			<a href="http://www.enable-javascript.com/" target="_blank">
        			En este enlace encontrarás instrucciones de cómo habilitar javascript en tu navegador</a>
        		</p>
  			</div>
  			</noscript>-->
  
        <script type="text/javascript" src="pw/assets/plugins/captcha/jquery.captcha.js"></script>
        <link href="pw/assets/plugins/captcha/captcha.css" rel="stylesheet" type="text/css" />
        <style type="text/css" media="screen">
          body { background-color: white; }
        </style>
        <script type="text/javascript" charset="utf-8">
         $(function() 
          {
            $(".ajax-fc-container").captcha(
            {
              borderColor: "#f2f2f2",
              text: "Arrastra la siguiente imagen al círculo : <span>lupa</span>"
            });
          });
        </script>
        <!--FIN CAPTCHA-->
  			
  			
        <!-- bootstrap 3.0.2 -->
        <link href="pw/assets/plugins/bootstrap3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="pw/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="pw/assets/plugins/AdminLTE/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
 	    		<link rel="shortcut icon" href="favicon.ico">
    </head>
    <body class="bg-white">

        <div class="form-box" id="login-box">
            <div class="header">Inicio de sesión</div>            
            <form id="loginForm" method="POST" action = "">            
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="usuario" class="form-control" placeholder="Escribe tu usuario "/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                    __CAPTCHA__
                    </div>
                    <div class="form-group">
                        __ERRORMSG__
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-orange btn-block">Iniciar sesión</button>
                    <p><a href="recover.php">Recuperar contraseña</a></p>                    
                </div>
                <input type="hidden" name="mode" value = "2">
            </form>
        </div>
    </body>
</html>
 	    
TEMPLATE;
 	

	$template["loginError"] = <<< TEMPLATE

	<div id= "loginError" class = "loginError"><i class="fa fa-times-circle"></i><span class = "color-orange"> __ERROR__</span></div>
	
TEMPLATE;

$template["captcha"] = <<< TEMPLATE
     
     <!-- Begin of captcha -->
     <div class="ajax-fc-container">Debes habilitar javaScript para ver la verificación Captcha</div>
     <!-- End of captcha -->
     
     	
TEMPLATE;
	
	
 		return $template[$name];
		
 	}
 }
?>