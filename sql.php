<?

/**
 * 
 * Clase encargada de generar y ejecutar los querys 
 * en la base de datos por default que es ORACLE
 * 
 */
class sql
{

	/**
	 * Objeto con la conexión activa para el sistema
	 * @var $connection Objeto con la conexión activa para el sistema
	 */	
	public $connection;
	
	
	/**
	 * 
	 * Constructor de la clase Security
	 * @param Object $security Objeto de seguridad
	 */
	function __construct()
	{	
		
		//include_once(dirname(__FILE__).'/'."funcionesMain.php");
	  //$this->funciones = new funcionesMain();
		
		/*$this->security = $security;
		if(!$this->security)
		{
			include_once(dirname(__FILE__).'/'."moduleSecurity.php");
	  	$this->security = new moduleSecurity();
		}*/	 	  	
	}


	/**
	 * 
	 * Regresa el resultado de una consulta dada a mano
	 * @param connection $connection Conexion a la base
	 * @param String $consulta Consulta a ejecutar
	 * @return Array con los resultados
	 */
	public function getPersonalData($connection, $consulta)
	{
	//	echo "Consulta: $consulta <br>";
	
	  //$this->doLog($consulta,2);
		if($consulta)
		{
			$stid = oci_parse($connection, $consulta);// or die ("Fall� la consulta");
			$resultado  = oci_execute( $stid );            
	//	$this->doLog("#Stid : $stid  Res: $resultado ",2);
 
	
          if($resultado)
		  {
			while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) 
			{							    
			  $res = $row;    	
	    	}
	    	oci_free_statement ($stid);	
			return $res;
          }
		  return false;
          
	    }
		return false;
	}
	
	
	
	
	/**
	 * Regresa el resultado de una consulta dada a mano
	 *
	 * @param connection $connection Conexion a la base
	 * @param String $consulta Consulta a ejecutar
	 * @return Array con los resultados
	 */
	public function getPersonalDataAux($connection, $consulta)
	{
	  
	  if($consulta)
	  {
		$stid = oci_parse($connection, $consulta);//or die ("Fallo la consulta");
        $resultado  = oci_execute( $stid );
        $res = array();
        if($resultado)
		{
		  
		  
		  
		  // Puede ser asi o OCI_ASSOC
		  while (($row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) != false) 
		  {							    
			$res[]= $row;    	
	      }	
		  
	      oci_free_statement ($stid);
		  	
		  return $res;
		}
		return false;
		
	  }
		return false;
	}
	
	public function getMultiplePersonalData($connection, $consulta)
	{
	//	echo "Consulta: $consulta <br>";
		if($consulta)
		{
			$resultado = mysql_query($consulta, $connection);// or die ("Fallo la consulta");
			
			if($resultado)
			{
				$results = mysql_fetch_array($resultado);

				if($results)
				{
					$results=mysql_fetch_array($resultado);
					do					
					{
						$res[] = $results;
					}while ($results);
					return $res;
				}
			}
			
			return false;
			
		}
		return "Error al pasar par�metros";
	}


/**
 * Genera y ejecuta la consulta para traer solo 1 registro(fila completa) de la tabla
 *
 * @param connection $connection Conexion a la base
 * @param String $tabla Nombre de la tabla de la base de datos
 * @param String $field Campo de la base de datos donde se va a buscar
 * @param unknown_type $value Valor a buscar del campo especificado
 * @param int $limit Limite de resultados
 * @return Regresa un array con los resultados
 */

  public function getSingleData($connection, $tabla, $field, $value)
	{
		
		if($field && $tabla && $value)
		{
			$consulta = "SELECT * from  $tabla WHERE $field = $value ";
			if($limit && is_int($limit))
			$consulta .= " LIMIT $limit";

			//echo "$consulta <br>";
			//die();
			
			$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta");
			$resultado  = oci_execute( $stid );            
			if($resultado)
			{

				while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) 
				{			
					$res[] = $row;    	
    		}	
					
				return $res;
			}
			return false;
			
		}
		return "Error al pasar parámetros";
	}
	
	
	/**
 * Genera y ejecuta la consulta para traer solo 1 registro de la tabla
 * especificando que columnas traer
 * @param connection $connection Conexion a la base
 * @param String $tabla Nombre de la tabla de la base de datos
 * @param Array $fields Array para campos a mostrar 
 * @param Array $condition Array de condiciones para el Where 
 * @param String $order Campo por el que se ordena la consulta
 * @param Array $operador Array con los operadores de AND, OR, NNOT, LIKE , etc...
 * @param Array $operation Array con los operadores entre campo-> dato (= , <=, >=, != , etc....)
 * @return Regresa un array con los resultados
 */
	 public function getSingleDataFields($connection, $tabla, $fields = false, $condition = false, $order = false, $operador=false, $operation= false)
	{

		if($tabla)
		{
			$consulta = "SELECT ";			
		
			if($fields)
			{
				$tamaño = sizeof($fields);
				$x = 1;

				foreach ($fields as $value)
				{
					
					if(strstr($value, "SUBSTRING"))
					{
						$consulta .= " $value ";
					}					
					else 
					{
						$consulta .= " $value ";
					}
					if($x < $tamaño)
					{
						$consulta .= ","	;
					}
					$x++;					
				}
			}

			else
			{
				$consulta .= " * " ;
			}
			
			$consulta .= " FROM $tabla ";
			
			if($condition)
			{
				$consulta .= " WHERE ";
				$x = 0;
				
				foreach ($condition as $field => $value)
				{
					if($x > 0)
					{
						if(!$operador[$x])
						{
							$consulta .= " AND ";
						}
						else
						{
							$consulta .= " $operador[$x] ";
						}
					}
					
					$consulta .= " ".trim($field)." ";
					if($operation[$x])
					{
						$consulta .= " $operation[$x] " ;
					}
					else
					{
						$consulta .= " = ";
					}

					$consulta .= " ? ";
					$x++;
				}
			}
			
		  if($order)
			{
				$consulta .= " ORDER BY $order ";
			}
			
		//	echo $consulta."<br>";
		//	die();
			
			try{
 

    	$stmt = $conn->prepare($consulta);
    	$stmt->execute(array($id));
    	$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
			catch (PDOException $e){
    	echo $e->getMessage();
			}
       
				
				/*$stid = oci_parse($connection, $consulta);
				$resultado  = oci_execute( $stid );
				$res = null;            
				if($resultado)
				{
					while (($row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) != false) 
					{			
						$res = $row;    	
    				}	
    		
    				return $res;
				}*/			
				$this->funciones->setError(41, "(getMultipleDataFields)");
				return false;
		}

		$this->funciones->setError(40, "(getMultipleDataFields)");
		return false;
	}


	/**
	 * Genera y ejecuta la consulta para traer multiples registros de la tabla
	 *
	 * @param connection $connection Conexion a la base de datos
	 * @param String $tabla Nombre de la tabla a buscar
	 * @param Array $condition Array con las diferentes condiciones a ejecutar
	 * @param int $limit Limite de resultados
	 * @param String $order Campo por que se quiere ordenar, debe de ser un campo valido
	 * @param Array $fields Array con los campos de la tabla a buscar, si no se pone, trae todos
	 * @return Regresa un Array de Arrays con los resultados de la consulta
	 */
	public function getMultipleData($connection, $tabla, $condition = false,  $limit = false, $order = false, $fields = false, $operation = false, $operador=false)
	{

	  
	
	$res = null;
		if($tabla)
		{
		//	echo "Hasta aqui 2 <br>";
			$consulta = "SELECT ";

			if($fields)
			{
				$tamaño = sizeof($fields);
				$x = 1;

				foreach ($fields as $value)
				{
					
				//	echo "Valor  : ".strstr($value, "SUBSTRING") . "<br>";
					
					if(strstr($value, "SUBSTRING"))
					{
						$consulta .= " $value ";
					}					
					else 
					{
						$consulta .= " $value ";
					}				

					if($x < $tamaño)
					{
						$consulta .= ","	;
					}
					$x++;					
				}
			}
			else
			{
				$consulta .= " * " ;
			}

			$consulta .= " from $tabla ";
      // echo "Hasta aqui";
			if($condition)
			{
				$consulta .= " WHERE ";
				$x = 0;
				
				foreach ($condition as $field => $value)
				{
				 // echo "$field : $value <br>";
					if($x > 0)
					{
						if(!$operador[$x])
						{
						$consulta .= " AND ";
						}
						else
						{
							$consulta .= " $operador[$x] ";
						}
					}
					$consulta .= " ".trim($field)." ";
					if($operation[$x])
					{
						$consulta .= " $operation[$x] " ;
					}
					else
					{
						$consulta .= " = ";
					}

					$consulta .= "$value";
					$x++;
				}
			}
			
			if($order)
			{
				$consulta .= " ORDER BY $order ";
			}

			if($limit)
			{
				$consulta .= " LIMIT $limit";
			}

		// echo "Consulta : <br> $consulta <br>";
			 //throw new Exception($consulta); 
			$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta");
			$resultado  = oci_execute( $stid );  
			          
		//	var_dump($resultado);

			if($resultado)
			{

				while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) 
				{			
					$res[] = $row;    	
    		}	
					
				return $res;
			}
			return false;
			
			
		}
		return false;
	}
	
	
	/**
	 * Genera y ejecuta la consulta para traer multiples registros de la tabla
	 * especificando que campos regresar
	 * @param connection $connection Conexion a la base de datos
	 * @param String $tabla Nombre de la tabla a buscar
	 * @param Array $fields Array con los campos de la tabla a buscar, si no se pone, trae todos
	 * @param Array $condition Array con las diferentes condiciones a ejecutar
	 * @param String $order Campo por que se quiere ordenar, debe de ser un campo valido
	 * @return Regresa un Array de Arrays con los resultados de la consulta
	 */
	
	public function getMultipleDataFields($connection, $tabla,  $fields = false, $condition = false, $order = false)
	{
		if($tabla)
		{
			$consulta = "SELECT ";
			
			if($fields)
			{
				$tamaño = sizeof($fields);
				$x = 1;

				foreach ($fields as $value)
				{
					$consulta .= " $value ";
					if($x < $tamaño)
					{
						$consulta .= ","	;
					}
					$x++;
				}
			}
			else
			{
				$consulta .= " * " ;
			}

			$consulta .= " from $tabla ";
      // echo "Hasta aqui";
			if($condition)
			{
				$consulta .= " WHERE ";
				$x = 0;

				foreach ($condition as $field => $value)
				{
					if($x > 0)
					$consulta .= " AND ";
					$consulta .= " $field = ? ";
					$x++;
				}
			}

			if($order)
			{
				$consulta .= " ORDER BY $order ";
			}
			
				
			echo "Consulta : <br> $consulta <br>";	
			die();		
			$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta");
			$resultado  = oci_execute( $stid );   

			if($resultado)
			{

				while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) 
				{			
					$res[] = $row;    	
    		}	
					
				return $res;
			}
			$this->funciones->setError(41, "(getMultipleDataFields)");
			return false;
			
		}
		
		$this->funciones->setError(40, "(getMultipleDataFields)");
		return false;
	}


	public function getMultipleDataAux($connection, $tabla, $condition = false,  $limit = false, $order = false, $operation = false, $boolean = false)
	{

		//echo "1 <br>";
		//if(!$connection)
		//$connection = $this->getConnection();
		if($tabla)
		{
			//echo "2 <br>";
			$consulta = "SELECT * from $tabla  ";
			if($condition)
			{
				$consulta .= " WHERE ";
				$x = 0;
				foreach ($condition as $field => $value)
				{
					if($x > 0){
						if($boolean[$x])
					      $consulta .= " $boolean[$x] ";
					    else
					      $consulta .= " AND ";
					}

				    if($x ==0){
					$consulta .= " $field ";
				    }
				    else{
				    	$consulta .= "`$field`";
				    }
					if($operation[$x])
					$consulta .= " $operation[$x] " ;
					else
					$consulta .= " = ";

					$consulta .= " $value ";
					$x++;
				}
			}
			if($order)
			$consulta .= " ORDER BY $order  ";

			if($limit && is_int($limit))
			$consulta .= " LIMIT $limit";

	//	echo "Cons : $consulta<br>";



			$resultado = mysql_query($consulta, $connection);// or die ("Fallo la consulta");
			if($resultado)
			{
				$results = mysql_fetch_array($resultado);
				if($results)
				{
					$results=mysql_fetch_array($resultado);
					do
					{
						$res[] = $results;
					}while ($results);

				}
				return $res;
				//mysql_close();
			}
			return false;
			
		}
		return false;
		
	}



/**
 * Realiza la actualización de una tabla con los campos que se le manden
 *
 * @param Connection $connection Conexion a la base
 * @param String $tabla Tabla a actualizar
 * @param String $datos Datos que se van a actualizar
 * @param Array $keyFields Array con los campos de la llave primaria
 * @param Array $operation Array con los operadores entre las llasves, =, != , <= ...etc...
 * @param Boolean $trasaction Bandera para ver si ejecutamos el query como transaccion
 * @return unknown
 */
  public function updateData($connection, $tabla, $datos, $keyFields, $operation= false,  $bitacora = false, $modulo = "" )
  {

  
	$consulta = "UPDATE $tabla SET $datos WHERE ";
		
	$x = 0;
	foreach ($keyFields as $field=>$value)
	{
	  if($x > 0)		  
	  $consulta .= " AND ";		
      if($operation[$x])
      {
        $consulta .= " $field $operation[$x] $value ";
      }
      else 
      {		  
	    $consulta .= " $field = $value ";
      }
		$x++;
	}
	//$this->doLog($consulta,2);
	//echo $consulta;
	
	$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta");
	
	$resultado  = oci_execute( $stid, OCI_COMMIT_ON_SUCCESS);
	
  if($bitacora)
	{
	  //	$this->insertaBitacora($connection, $modulo, $tabla, "Update", $consulta);
	}
	//oci_commit($connection);
	return $resultado;
   		
  }




  public function insertData($tabla, $fields, $datos, $connection, $bitacora = false, $modulo = "")
  {

	$consulta = "INSERT INTO  $tabla ($fields) values ($datos) ";
 //throw new Exception($consulta);
	
	$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta ");
	
	$resultado  = oci_execute( $stid, OCI_COMMIT_ON_SUCCESS );
	
    if($bitacora)
	{
	  //	$this->insertaBitacora($connection, $modulo, $tabla, "Insert", $consulta);
	}
	
	
	return $resultado;
  }

  
  
  public function deleteMultipleData($connection,$tabla, $condition, $operation = false, $operador=false,$bitacora = false, $modulo = "")
  {

	$consulta = "DELETE FROM $tabla WHERE";		
	$x = 0;
	      
	if($condition)
	{
	  foreach ($condition as $field => $value)
	 {
			    
			    if($x > 0)
				{
				  if(!$operador[$x])
				  {
					$consulta .= " AND ";
				  }
				  else
				  {
				    
					$consulta .= " $operador[$x] ";
				  }
				}
				$consulta .= " ".trim($field)." ";
				if($operation[$x])
				{
				  $consulta .= " $operation[$x] " ;
				}
				else
				{
				  $consulta .= " = ";
				}
                $consulta .= "$value";
				$x++;
			}
		}
		
		else 
		{ 
		    if($x > 0)		  
	      $consulta .= " AND ";
	      $consulta .= " $field = $value ";          
		  $x++;
		  
		}
		
      $stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta ");
	
	  $resultado  = oci_execute( $stid, OCI_COMMIT_ON_SUCCESS );
	
	  if($bitacora)
	  {
	    //	$this->insertaBitacora($connection, $modulo, $tabla, "Delete", $consulta);
	  }
		

	}
  
  
  
  public function deleteData($connection,$tabla, $key, $field)
	{

		$consulta = "DELETE FROM `$tabla` WHERE `$key`=$field";
	//	return $consulta;
		if($tabla && $key)

		{

			if(mysql_query($consulta, $connection))

			return true;

			else

			return false;

		}

		return false;

	}
	
	/**
	 * 
	 * Funcion para borrar campo don una key de varios campos
	 * @param Connection $connection La conexion a la base
	 * @param String $tabla La tabla a borrar
	 * @param Array $fields Array con los valores para generar la llave en formato Campo=>valor, si es Strin usar ''
	 */
  public function deleteFieldsData($connection,$tabla,$fields, $bitacora = false, $modulo = "")

	{

		$consulta = "DELETE FROM $tabla WHERE";		
				
	    $x = 0;
	    foreach ($fields as $field=>$value)
	    {
	      if($x > 0)		  
	      $consulta .= " AND ";
	      $consulta .= " $field = $value ";          
		  $x++;
	}
		
	$stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta ");
	
	$resultado  = oci_execute( $stid, OCI_COMMIT_ON_SUCCESS );
	
	if($bitacora)
	{
	  //	$this->insertaBitacora($connection, $modulo, $tabla, "Delete", $consulta);
	}
	
	return $resultado;

	}
	
	
	
	public function doLog($variable, $mode)
	{
		$myFile = "sqlLog.txt";
		$data= "";
		switch ($mode)
		{
				case 1:
						foreach ($variable as $key=>$value)
						{
							$data .= "Key [$key] - Value [$value] \r\n";
							break;
						}
						
					case 2:
					{
							$data .= $variable;
							break;					
						
					}
					
					case 3:
					{
							$data .= var_dump($variable);
							break;					
						
					}
					
					case 4:
					
						
						foreach($variable as $nombre_campo => $valor){ 
   									$data .= "\$" . $nombre_campo . "='" . $valor . "';"; 
   							eval($data); 
						} 
						break;
					
					
						
						
			
		}
	
		$fh = fopen($myFile, 'w') ;//or die("can't open file");
		fwrite($fh, $data);
		fclose($fh);

	}	
	
	public function insertaBitacora($connection, $modulo, $tabla, $accion, $datos, $loginFlag = false)
	{
		
	  if($loginFlag)
	  {
	  	$usuario = 0;	  
	  }	
	  else
	  {
	  	$usuario = $this->security->decryptVariable(2, "cveUsuario");
	  }
		
	  $ip = $_SERVER['REMOTE_ADDR'];
	  $datos = preg_replace("/'/", "''", $datos);
	  
	  $consulta = "INSERT INTO ** (USUARIO, FECHA, MODULO, TABLA, ACCION, DATOS, SISTEMA, IP) 
	  values ('".$usuario."', SYSDATE, '$modulo', '$tabla', '$accion', '$datos', 'BCRT', '$ip') ";
	 // echo $consulta;
	  //die();
	  $stid = oci_parse($connection, $consulta);// or die ("Fallo la consulta ");
	  oci_execute( $stid, OCI_COMMIT_ON_SUCCESS );
	
	  
	//return $resultado;
	  
	}

}


?>