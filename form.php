<?php

/**
 * Clase encargada de ejecutar funciones para formas html
 * en automático
 * @author pcalzada
 *
 */
class form extends funciones
{
    
    /**
     * Nombre de la clase
     * @var String  - Nombre de la clase 
     */
    //private $className;
    

    function __construct()
    {
        //$this->className = "form";
    }
    
    /** 	 
     * Función que genera un combo con los valores dados por un array
     * @param String 	$name					Nombre del combo
     * @param Array		$arrValues				Array con los valores que vamos ausar, debe de estar en la forma $key=>$value
     * @param Bolean	$space					Si se quiere un espacio al inicio del combo
     * @param Integer 	$value					Valor del combo preseleccionado
     * @param String	$formName				Nombre de la forma para enviar en onChangeSubmit
     * @param Boolean	$onChangeSubmit			Nos dice si se envia o no el formulario al tener un cambio
     * @param String 	$onChangeFunction		Nombre de la función en JS que llamamos al tener un cambio
     * @param Boolean	$todos					Bandera para poner espacio en blanco o la palabra Todos al principio del comobo
     */
    
    public function getArraySelect($name, $arrValues, $space = false, $value = false, $formName = false, $onChangeSubmit = false, $onChangeFunction = false, $class = "", $todos = false, $extraParams = false)
    {
        
        $select = $this->getTemplate ( "select" );
        $select = preg_replace ( "/__CLASS__/", $class, $select );
        $select = preg_replace ( "/__NAME__/", $name, $select );
        $templateOptions = $this->getTemplate ( "option" );
        
        $options = "";
        if ($space)
        {
            $options = $this->getTemplate ( "nullOption" );
        }
        
        if($todos && !$space){
        	$options .= $this->getTemplate ( "todosOption" );
        }
        
        
        $select = preg_replace ( "/__EXTRAPARAMS__/", $extraParams ? $extraParams : "", $select );        
        
        
        
        if(!$arrValues)
        {
            $this->setError ( 14, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
            $select = preg_replace ( "/__OPTIONS__/", "", $select );
            $select = preg_replace ( "/__ONCHANGE__/", "", $select );            
            return $select;
        }
        
        
        foreach ( $arrValues as $key => $dataValue )
        {
            $aux = $templateOptions;
            $aux = preg_replace ( "/__VALUE__/", $key, $aux );
            $aux = preg_replace ( "/__SELECTED__/", $key == $value ? " SELECTED" : "", $aux );
            $aux = preg_replace ( "/__TEXT__/", $dataValue, $aux );
            $options .= $aux . "\n";
        }
        
        $select = preg_replace ( "/__OPTIONS__/", $options, $select );
        
        $changeSubmit = "";
        if ($onChangeFunction)
        {
            $changeSubmit = $this->getTemplate ( "onChange" );
            $changeSubmit = preg_replace ( "/__FUNCTION__/", $onChangeFunction, $changeSubmit );
        }
        
        if ($onChangeSubmit)
        {
            $changeSubmit = $this->getTemplate ( "changeSubmit" );
            $changeSubmit = preg_replace ( "/__FORMNAME__/", $formName, $changeSubmit );
        }
        
        $select = preg_replace ( "/__ONCHANGE__/", $changeSubmit, $select );
        
        
        
        /*if($onChangeSubmit)
		{		  
		  $select = preg_replace("/__ONCHANGE__/", "onChange='document.$formName.submit()'", $select);
		}
  	*/
        
        return $select;
    }
    
    
    
    /**
     * Genera el código para crear un select con campos de una base de datos. 
     * Puede llamar a una función en js para hacerlo dinámico
     * 
     * @param Object	$mainObj		Objeto principal del sistema
     * @param String	$table			Tabla a la que se conecta
     * @param String 	$name			Nombre que va a recibir el campo
     * @param String 	$key			Llave para el select
     * @param String 	$fld			Campo que mostrara el texto del select
     * @param Boolean	$space			Si se quiere un espacio al inicio del combo
     * @param boolean 	$selected 		Condición para que aparezca preselecionado algun elemento, puede ser un valor o un array de valores
     * @param String 	$order 			Campo para hacer el ordenamiento del select
     * @param Array 	$condition 		Array con las condiciones para el query
     * @param String 	$onChange 		Si existe, llama una funcion en js al cambiar
     * @param String 	$class 			La clase CSS para agregar al select
     * @param String 	$subString 		Limita el tamaño del texto a mostrar en el select	
     *
     * @return Regresa el html necesario para el select
     */
    public function getSelect($mainObj, $table, $name, $key, $fld, $space = false, $selected = false, $order = false, $condition = null, $onChange = false, $class = "", $substring = false, $extraParams = false, $todos = false)
    {

        if ($table)
        {
            $fields = array ($key, $fld );

            $sqlData = $mainObj->sql->executeQuery ( $mainObj->connection, $table, $fields, $condition, $order );
            
            if ($sqlData)
            {
                
                $select = $this->getTemplate ( "select" );
                $select = preg_replace ( "/__CLASS__/", $class, $select );
                $change = "";
                if ($onChange)
                {
                    $change = $this->getTemplate ( "onChange" );
                    $change = preg_replace ( "/__FUNCTION__/", $change, $select );
                }
                
                $select = preg_replace ( "/__ONCHANGE__/", $change, $select );
                
                $select = preg_replace ( "/__NAME__/", $name, $select );
                
                $select = preg_replace ( "/__EXTRAPARAMS__/", $extraParams ? $extraParams : "", $select );
                
                $templateOptions = $this->getTemplate ( "option" );
                
                $options = "";
                if ($space == true)
                {
                    
                    if($todos != false)
                    {
                        
                         $val =  $this->getTemplate ( "todosOptionVal" );
                         $val = preg_replace("/__VAL__/", $todos, $val);
                         $options .= $val;
                        
                    }
                    else
                    {
                        $options .= $this->getTemplate ( "nullOption" );
                    }
                    
                }
                
                foreach ( $sqlData as $field )
                {
                    
                    $field = $this->getArrayObject ( $mainObj->conId, $field );
                    $aux = $templateOptions;
                   // $aux = preg_replace ( "/__CLASS__/", $class, $aux );
                    $aux = preg_replace ( "/__VALUE__/", $field [$key], $aux );
                    //$aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );

                    if($selected)
                    {
                        if(is_array($selected))
                        {
                            $aux = preg_replace ( "/__SELECTED__/",  in_array($field [$key], $selected)? " SELECTED" : "", $aux );
                            
                        }
                        else
                        {
                            $aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                            
                        }
                                                
                    }
                    else
                    {
                        $aux = preg_replace ( "/__SELECTED__/", "", $aux );
                        
                    }
                    
                    
                    //$aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                    if ($substring)
                    {
                        $aux = preg_replace ( "/__TEXT__/", substr ( rawurldecode ( $field [$fld] ), 0, $substring ), $aux );
                    }
                    else
                    {
                        $aux = preg_replace ( "/__TEXT__/", rawurldecode ( $field [$fld] ), $aux );
                    }
                    $options .= $aux . "\n";
                }
                $select = preg_replace ( "/__OPTIONS__/", $options, $select );
                
                return $select;
            }
            else
            {
                $this->setError ( 112, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
            }
        }
        
        $this->setError ( 111, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
        
        return "";
    }
    
    
    
 /**
     * Genera el código de los elementos de un select con campos de una base de datos. 
     * Puede llamar a una función en js para hacerlo dinámico
     * 
     * @param Object	$mainObj		Objeto principal del sistema
     * @param String	$table			Tabla a la que se conecta
     * @param String 	$key			Llave para el select
     * @param String 	$fld			Campo que mostrara el texto del select
     * @param Boolean	$space			Si se quiere un espacio al inicio del combo
     * @param boolean 	$selected 		Condición para que aparezca preselecionado algun elemento, puede ser un valor o un array de valores
     * @param String 	$order 			Campo para hacer el ordenamiento del select
     * @param Array 	$condition 		Array con las condiciones para el query
     * @param String 	$subString 		Limita el tamaño del texto a mostrar en el select	
     *
     * @return Regresa el html necesario para el select
     */
    public function getSelectOptions($mainObj, $table,  $key, $fld, $space = false, $selected = false, $order = false, $condition = null,   $substring = false)
    {
        if ($table)
        {
            $fields = array ($key, $fld );

            $sqlData = $mainObj->sql->executeQuery ( $mainObj->connection, $table, $fields, $condition, $order );
            
            if ($sqlData)
            {
                
                $templateOptions = $this->getTemplate ( "option" );
                
                $options = "";
                if ($space == true)
                {
                    $options .= $this->getTemplate ( "nullOption" );
                }
                
                foreach ( $sqlData as $field )
                {
                    
                    $field = $this->getArrayObject ( $mainObj->conId, $field );
                    $aux = $templateOptions;
                   // $aux = preg_replace ( "/__CLASS__/", $class, $aux );
                    $aux = preg_replace ( "/__VALUE__/", $field [$key], $aux );
                    //$aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );

                    if($selected)
                    {
                        if(is_array($selected))
                        {
                            $aux = preg_replace ( "/__SELECTED__/",  in_array($field [$key], $selected)? " SELECTED" : "", $aux );
                            
                        }
                        else
                        {
                            $aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                            
                        }
                                                
                    }
                    else
                    {
                        $aux = preg_replace ( "/__SELECTED__/", "", $aux );
                        
                    }
                    
                    
                    //$aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                    if ($substring)
                    {
                        $aux = preg_replace ( "/__TEXT__/", substr ( rawurldecode ( $field [$fld] ), 0, $substring ), $aux );
                    }
                    else
                    {
                        $aux = preg_replace ( "/__TEXT__/", rawurldecode ( $field [$fld] ), $aux );
                    }
                    $options .= $aux . "\n";
                }
                
                
                return $options;
            }
            else
            {
                $this->setError ( 112, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
            }
        }
        
        $this->setError ( 111, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
        
        return "";
    }
    
/**
     * Genera el código para crear un select con campos de una base de datos. 
     * Puede llamar a una función en js para hacerlo dinámico
     * 
     * @param Object	$mainObj		Objeto principal del sistema
     * @param String	$consulta		Consulta a ejecutar
     * @param Array		$params			Parámetros de la consulta
     * @param String 	$name			Nombre que va a recibir el campo
     * @param String 	$key			Llave para el select
     * @param String 	$fld			Campo que mostrara el texto del select
     * @param Boolean	$space			Si se quiere un espacio al inicio del combo
     * @param boolean 	$selected 		Condición para que aparezca preselecionado algun elemento
     * @param String 	$order 			Campo para hacer el ordenamiento del select
     //* @param Array 	$condition 		Array con las condiciones para el query
     * @param String 	$onChange 		Si existe, llama una funcion en js al cambiar
     * @param String 	$class 			La clase CSS para agregar al select
     * @param String 	$subString 		Limita el tamaño del texto a mostrar en el select	
     *
     * @return Regresa el html necesario para el select
     */
    public function getQuerySelect($mainObj, $consulta, $params, $name, $key, $fld, $space = false, $selected = false, $order = false, $onChange = false, $class = "", $substring = false, $extraParams = false, $todos = false)
    {
        
        if ($consulta)
        {
            
            $fields = array ($key, $fld );

            //$sqlData = $mainObj->sql->executeQuery ( $mainObj->connection, $table, $fields, $condition );
            $ps = $mainObj->sql->setSimpleQuery( $mainObj->connection, $consulta );
            $sqlData = $mainObj->sql->executeSimpleQuery( $ps, $params, $consulta, null, false, true);
	  
            
            if ($sqlData)
            {
                
                $select = $this->getTemplate ( "select" );
                $select = preg_replace ( "/__CLASS__/", $class, $select );
                $change = "";
                if ($onChange)
                {
                    $change = $this->getTemplate ( "onChange" );
                    $change = preg_replace ( "/__FUNCTION__/", $change, $select );
                }
                
                $select = preg_replace ( "/__ONCHANGE__/", $change, $select );
                
                $select = preg_replace ( "/__NAME__/", $name, $select );
                //$select = preg_replace ( "/__EXTRAPARAMS__/", "", $select );
                $select = preg_replace ( "/__EXTRAPARAMS__/", $extraParams ? $extraParams : "", $select );
                
                $templateOptions = $this->getTemplate ( "option" );
                
                $options = "";
                if ($space == true)
                {
                    //$options .= $this->getTemplate ( "nullOption" );
                    
                    if($todos != false)
                    {
                        
                        $val =  $this->getTemplate ( "todosOptionVal" );
                        $val = preg_replace("/__VAL__/", $todos, $val);
                        $options .= $val;
                        
                    }
                    else
                    {
                        $options .= $this->getTemplate ( "nullOption" );
                    }
                    
                }
                
                foreach ( $sqlData as $field )
                {
                    
                    $field = $this->getArrayObject ( $mainObj->conId, $field );
                    $aux = $templateOptions;
                    $aux = preg_replace ( "/__CLASS__/", $class, $aux );
                    $aux = preg_replace ( "/__VALUE__/", $field [$key], $aux );
                    //$aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                   if($selected)
                    {
                        if(is_array($selected))
                        {
                            $aux = preg_replace ( "/__SELECTED__/",  in_array($field [$key], $selected)? " SELECTED" : "", $aux );
                            
                        }
                        else
                        {
                            $aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                            
                        }
                                                
                    }
                    else
                    {
                        $aux = preg_replace ( "/__SELECTED__/", "", $aux );
                        
                    }
                    if ($substring)
                    {
                        $aux = preg_replace ( "/__TEXT__/", substr ( rawurldecode ( $field [$fld] ), 0, $substring ), $aux );
                    }
                    else
                    {
                        $aux = preg_replace ( "/__TEXT__/", rawurldecode ( $field [$fld] ), $aux );
                    }
                    $options .= $aux . "\n";
                }
                $select = preg_replace ( "/__OPTIONS__/", $options, $select );
                
                return $select;
            }
            else
            {
                $this->setError ( 112, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
            }
        }
        
        $this->setError ( 111, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
        
        return "";
    }
    
    /**
     * Genera el código para crear un select con campos de una base de datos. 
     * Puede llamar a una función en js para hacerlo dinámico
     * 
     * @param Object	$mainObj		Objeto principal del sistema
     * @param String	$table			Tabla a la que se conecta
     * @param Array		$fields			Array con los campos a buscar 
     * @param Array 	$condition 		Array con las condiciones para el query
     * @param String 	$key			Llave para el select
     * @param String 	$fld			Campo que mostrara el texto del select

     * @return Regresa el html necesario para <options>
     */
    public function getOptionList($mainObj, $table, $fields, $condition, $key, $fld, $operation, $selected = false)
    {
        

        $sqlData = $mainObj->sql->executeQuery ( $mainObj->connection, $table, $fields, $condition, false, $operation );
        
        
        if ($sqlData)
        {
            
            $templateOptions = $this->getTemplate ( "option" );
            
            $options = "";

            
            foreach ( $sqlData as $field )
            {
                
                $field = $this->getArrayObject ( $mainObj->conId, $field );
                $aux = $templateOptions;
                $aux = preg_replace ( "/__CLASS__/", "", $aux );
                $aux = preg_replace ( "/__VALUE__/", $field [$key], $aux );
                $aux = preg_replace ( "/__TEXT__/", rawurldecode ( $field [$fld] ), $aux );
                $aux = preg_replace ( "/__SELECTED__/", $field [$key] == $selected ? " SELECTED" : "", $aux );
                $options .= $aux . "\n";
            }            
            return $options;
        }
        else
        {
            $this->setError ( 113, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
            return "false";
        }
        
        $this->setError ( 111, __LINE__ . "::" . __FUNCTION__, __CLASS__, 2 );
        return "false";
    
    }
    
    private function getTemplate($name)
    {
        $template ["select"] = <<< TEMP
<select name= "__NAME__" id= "__NAME__" __ONCHANGE__ class = "__CLASS__"  __EXTRAPARAMS__>__OPTIONS__</select>
TEMP;
        
        $template ["option"] = <<< TEMP
<option value = "__VALUE__"  __SELECTED__ >__TEXT__</option>
TEMP;
        
        $template ["nullOption"] = <<< TEMP
<option value = ''></option>
TEMP;

        $template ["todosOption"] = <<< TEMP
<option value = ''>Todos</option> 
TEMP;
        
        
        $template ["todosOptionVal"] = <<< TEMP
<option value = ''>__VAL__</option>
TEMP;
        
        
        $template ["onChange"] = <<< TEMP
onChange="__FUNCTION__(0)"
TEMP;
        
        $template ["changeSubmit"] = <<< TEMP
onChange="document.__FORMNAME__.submit()"
TEMP;
        
        /*	$template["selectOnChange"] = <<< TEMP
			    onchange="mostrar(this)"
TEMP;
	
		$template["getFondos"] = <<< TEMP
			    onchange="getFondos(0)"
TEMP;
	*/
        
        return $template [$name];
    }

}
?>